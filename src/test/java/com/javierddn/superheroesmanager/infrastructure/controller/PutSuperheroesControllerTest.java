package com.javierddn.superheroesmanager.infrastructure.controller;

import com.javierddn.superheroesmanager.domain.entity.SuperheroEntity;
import com.javierddn.superheroesmanager.domain.port.UpdateSuperheroesService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;

import java.util.UUID;

import static org.mockito.Mockito.doReturn;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PutSuperheroesControllerTest {

    @MockBean
    private UpdateSuperheroesService updateSuperheroesService;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void givenSuperheroAndNewNameThenUpdateSuperheroName() {
        // Given
        UUID id = UUID.randomUUID();
        SuperheroEntity updatedSuperhero = new SuperheroEntity.Builder().id(id).name("Arrow").build();
        updatedSuperhero.setName("Green Arrow");
        doReturn(updatedSuperhero).when(updateSuperheroesService).update(updatedSuperhero);

        // When
        restTemplate.put("/superhero", updatedSuperhero, SuperheroEntity.class);

        // Then
    }

}