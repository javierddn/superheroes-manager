package com.javierddn.superheroesmanager.infrastructure.controller;

import com.javierddn.superheroesmanager.domain.entity.SuperheroEntity;
import com.javierddn.superheroesmanager.domain.port.GetSuperheroesService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.doReturn;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class GetSuperheroesControllerTest {

    @MockBean
    private GetSuperheroesService getSuperheroesService;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void givenNothingThenReturnAllSuperheroes() {
        // Given

        // When
        ResponseEntity<List> result = restTemplate.getForEntity("/superheroes", List.class);

        // Then
        assertThat(result.getStatusCode(), is(HttpStatus.OK));
        List superheroesList = result.getBody();
        assertThat(superheroesList, is(not(nullValue())));
    }

    @Test
    void givenExistingSuperheroIdThenReturnSuperhero() {
        // Given
        UUID id = UUID.randomUUID();
        SuperheroEntity superheroEntity = new SuperheroEntity.Builder().id(id).name("Shiva").build();
        Mockito.doReturn(superheroEntity).when(getSuperheroesService).findById(id);

        // When
        ResponseEntity<SuperheroEntity> result = restTemplate.getForEntity("/superhero/{id}", SuperheroEntity.class, id);

        // Then
        assertThat(result.getStatusCode(), is(HttpStatus.OK));
        SuperheroEntity superhero = result.getBody();
        assertThat(superhero, is(not(nullValue())));
        assertThat(superhero.getId(), is(id));
    }

    @Test
    void givenStringThenReturnAllSuperherosWithNamesThatContainIt() {
        // Given
        String fragmentOfName = "man";
        SuperheroEntity superman = new SuperheroEntity.Builder().id(UUID.randomUUID()).name("Superman").build();
        SuperheroEntity batman = new SuperheroEntity.Builder().id(UUID.randomUUID()).name("Batman").build();
        SuperheroEntity manoloElGrande = new SuperheroEntity.Builder().id(UUID.randomUUID()).name("Manolo el Grande").build();
        List<SuperheroEntity> superheroEntityListMock = Arrays.asList(superman, batman, manoloElGrande);
        doReturn(superheroEntityListMock).when(getSuperheroesService).findAllWithStringInName(fragmentOfName);

        // When
        ResponseEntity<List> result = restTemplate.getForEntity("/superheroes/{fragmentOfName}", List.class, fragmentOfName);

        // Then
        assertThat(result.getStatusCode(), is(HttpStatus.OK));
        List superheroesList = result.getBody();
        assertThat(superheroesList, is(not(nullValue())));
    }
}