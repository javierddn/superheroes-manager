package com.javierddn.superheroesmanager.infrastructure.controller;

import com.javierddn.superheroesmanager.domain.entity.SuperheroEntity;
import com.javierddn.superheroesmanager.domain.port.CreateSuperheroesService;
import com.javierddn.superheroesmanager.domain.port.GetSuperheroesService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.doReturn;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PostSuperheroesControllerTest {

    @MockBean
    private CreateSuperheroesService createSuperheroesService;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void givenNewSuperheroNameThenSaveSuperhero() {
        // Given
        String newSuperheroName = "Flash";
        UUID id = UUID.randomUUID();
        SuperheroEntity superheroEntity = new SuperheroEntity.Builder().id(id).name(newSuperheroName).build();
        doReturn(superheroEntity).when(createSuperheroesService).save(superheroEntity);

        // When
        ResponseEntity<SuperheroEntity> result = restTemplate.postForEntity("/superhero", superheroEntity, SuperheroEntity.class);

        // Then
        assertThat(result.getStatusCode(), is(HttpStatus.OK));
        SuperheroEntity superhero = result.getBody();
        assertThat(superhero, is(not(nullValue())));
        assertThat(superhero.getId(), is(id));
        assertThat(superhero.getName(), is(newSuperheroName));
    }

}