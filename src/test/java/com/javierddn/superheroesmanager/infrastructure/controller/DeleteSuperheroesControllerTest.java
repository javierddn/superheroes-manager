package com.javierddn.superheroesmanager.infrastructure.controller;

import com.javierddn.superheroesmanager.domain.port.RemoveSuperheroesService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class DeleteSuperheroesControllerTest {

    @MockBean
    private RemoveSuperheroesService removeSuperheroesService;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void givenExistingSuperheroIdThenDeleteItSendingId() {
        // Given
        UUID id = UUID.randomUUID();
        doNothing().when(removeSuperheroesService).remove(id);

        // When
        restTemplate.delete("/superhero/{id}", id);

        // Then
    }

}