package com.javierddn.superheroesmanager.infrastructure.liquibase;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.system.CapturedOutput;
import org.springframework.boot.test.system.OutputCaptureExtension;
import org.springframework.test.context.ActiveProfiles;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("test")
@SpringBootTest
@ExtendWith(OutputCaptureExtension.class)
class LiquibaseTests {

    @Test
    void givenSetTestSpringBootProfileThenLogOutputHasConfirmationMessages(CapturedOutput output) {
        //Given
        String usingTestSpringBootProfileMessage = "The following profiles are active: test";

        // When

        // Then
        assertThat(output).contains(usingTestSpringBootProfileMessage);
    }

    @DisplayName("givenInitializedDatabaseThenLogOutputHasChangeLogLockMessage")
    @Test
    void givenInitializedDatabaseThenLogOutputHasChangeLogLockMessage(CapturedOutput output) {
        //Given
        String lockedDatabaseMessage = "Successfully acquired change log lock";

        // When
        // Application starts

        // Then
        assertThat(output).contains(lockedDatabaseMessage);
    }

    @Test
    void givenExecutedInitialChangelogThenLogOutputHasConfirmationMessages(CapturedOutput output) {
        //Given
        String createdSuperheroesTableMessage = "Table superheroes created";
        String insertedSuperheroesMessage = "New row inserted into superheroes";

        // When
        // Changelog executed

        // Then
        assertThat(output).contains(createdSuperheroesTableMessage).contains(insertedSuperheroesMessage);
    }

    @Test
    void givenLoadedChangelogThenLogOutputHasreleasedChangeLogLockMessage(CapturedOutput output) {
        //Given
        String releasedDatabaseLockMessage = "Successfully released change log lock";

        // When
        // Changelog Loaded

        // Then
        assertThat(output).contains(releasedDatabaseLockMessage);
    }

}
