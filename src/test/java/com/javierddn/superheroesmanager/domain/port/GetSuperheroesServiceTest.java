package com.javierddn.superheroesmanager.domain.port;

import com.javierddn.superheroesmanager.domain.entity.SuperheroEntity;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;

import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
class GetSuperheroesServiceTest {

    @MockBean
    SuperheroesRepository repository;

    @Autowired
    GetSuperheroesService getSuperheroesService;

    @Test
    void givenNothingThenReturnAllSuperheroes() {
        // Given
        UUID id = UUID.randomUUID();
        doReturn(Collections.singletonList(new SuperheroEntity.Builder().id(id).name("Superman").build())).when(repository).findAll();

        // When
        List<SuperheroEntity> superheroesList = getSuperheroesService.getAll();

        // Then
        assertThat(superheroesList, is(not(nullValue())));
        assertThat(superheroesList, is(not(empty())));
        SuperheroEntity superhero = superheroesList.get(0);
        assertThat(superhero.getId(), is(not(nullValue())));
        assertThat(superhero.getId(), is(id));
        assertThat(superhero.getName(), is(not(nullValue())));
        assertThat(superhero.getName(), is("Superman"));
    }

    @Test
    void givenExistingSuperheroIdThenReturnSuperhero() {
        // Given
        UUID id = UUID.randomUUID();
        Optional<SuperheroEntity> optionalSuperhero = Optional.of(new SuperheroEntity.Builder().id(id).name("Superman").build());
        doReturn(optionalSuperhero).when(repository).findById(id);

        // When
        SuperheroEntity superhero = getSuperheroesService.findById(id);

        // Then
        assertThat(superhero, is(not(nullValue())));
        assertThat(superhero.getId(), is(id));
        assertThat(superhero.getName(), is("Superman"));

        ArgumentCaptor<UUID> idCaptor = ArgumentCaptor.forClass(UUID.class);
        verify(repository).findById(idCaptor.capture());
        assertThat(idCaptor.getValue(), is(id));
    }

    @Test
    void givenStringThenReturnAllSuperherosWithNamesThatContainIt() {
        // Given
        String fragmentOfName = "man";
        SuperheroEntity superman = new SuperheroEntity.Builder().id(UUID.randomUUID()).name("Superman").build();
        SuperheroEntity batman = new SuperheroEntity.Builder().id(UUID.randomUUID()).name("Batman").build();
        SuperheroEntity thor = new SuperheroEntity.Builder().id(UUID.randomUUID()).name("Thor").build();
        SuperheroEntity manoloElGrande = new SuperheroEntity.Builder().id(UUID.randomUUID()).name("Manolo el Grande").build();
        List<SuperheroEntity> superheroEntityListMock = Arrays.asList(superman, batman, thor, manoloElGrande);
        doReturn(superheroEntityListMock).when(repository).findAll();

        // When
        List<SuperheroEntity> superheroesList = getSuperheroesService.findAllWithStringInName(fragmentOfName);

        // Then
        assertThat(superheroesList, is(not(nullValue())));
        assertThat(superheroesList, hasSize(3));
        assertThat(superheroesList, hasItem(hasProperty("name", is("Superman"))));
        assertThat(superheroesList, hasItem(hasProperty("name", is("Batman"))));
        assertThat(superheroesList, hasItem(hasProperty("name", is("Manolo el Grande"))));

    }

}