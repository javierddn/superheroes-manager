package com.javierddn.superheroesmanager.domain.port;

import com.javierddn.superheroesmanager.domain.entity.SuperheroEntity;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
class UpdateSuperheroesServiceTest {

    @MockBean
    SuperheroesRepository repository;

    @Autowired
    UpdateSuperheroesService updateSuperheroesService;

    @Test
    void givenSuperheroAndNewNameThenUpdateSuperheroName() {
        // Given
        UUID id = UUID.randomUUID();
        SuperheroEntity superheroMock = new SuperheroEntity.Builder().id(id).name("Arrow").build();
        superheroMock.setName("Green Arrow");
        doReturn(superheroMock).when(repository).save(superheroMock);

        // When
        SuperheroEntity updatedSuperhero = updateSuperheroesService.update(superheroMock);

        // Then
        assertThat(updatedSuperhero, is(not(nullValue())));
        assertThat(updatedSuperhero.getId(), is(not(nullValue())));
        assertThat(updatedSuperhero.getId(), is(id));
        assertThat(updatedSuperhero.getName(), is(not(nullValue())));
        assertThat(updatedSuperhero.getName(), is("Green Arrow"));

        ArgumentCaptor<SuperheroEntity> idCaptor = ArgumentCaptor.forClass(SuperheroEntity.class);
        verify(repository).save(idCaptor.capture());
        assertThat(idCaptor.getValue(), is(updatedSuperhero));
    }

}