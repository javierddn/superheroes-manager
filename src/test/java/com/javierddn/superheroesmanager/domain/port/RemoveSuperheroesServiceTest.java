package com.javierddn.superheroesmanager.domain.port;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;

import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
class RemoveSuperheroesServiceTest {

    @MockBean
    SuperheroesRepository repository;

    @Autowired
    RemoveSuperheroesService removeSuperheroesService;

    @Test
    void givenExistingSuperheroIdThenRemoveItSendingId() {
        // Given
        UUID id = UUID.randomUUID();
        doNothing().when(repository).deleteById(id);

        // When
        removeSuperheroesService.remove(id);

        // Then
        ArgumentCaptor<UUID> idCaptor = ArgumentCaptor.forClass(UUID.class);
        verify(repository).deleteById(idCaptor.capture());
        assertThat(idCaptor.getValue(), is(id));
    }

}