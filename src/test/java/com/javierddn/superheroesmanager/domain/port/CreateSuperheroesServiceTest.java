package com.javierddn.superheroesmanager.domain.port;

import com.javierddn.superheroesmanager.domain.entity.SuperheroEntity;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
class CreateSuperheroesServiceTest {

    @MockBean
    SuperheroesRepository repository;

    @Autowired
    CreateSuperheroesService createSuperheroesService;

    @Test
    void givenNewSuperheroNameThenSaveSuperhero() {
        // Given
        String newSuperheroName = "Flash";
        UUID id = UUID.randomUUID();
        SuperheroEntity superheroEntityMock = new SuperheroEntity.Builder().id(id).name(newSuperheroName).build();
        doReturn(superheroEntityMock).when(repository).save(superheroEntityMock);

        // When
        SuperheroEntity superheroEntity = createSuperheroesService.save(superheroEntityMock);

        // Then
        assertThat(superheroEntity, is(not(nullValue())));
        assertThat(superheroEntity.getId(), is(id));
        assertThat(superheroEntity.getName(), is("Flash"));

        ArgumentCaptor<SuperheroEntity> idCaptor = ArgumentCaptor.forClass(SuperheroEntity.class);
        verify(repository).save(idCaptor.capture());
        assertThat(idCaptor.getValue(), is(superheroEntity));
    }

}