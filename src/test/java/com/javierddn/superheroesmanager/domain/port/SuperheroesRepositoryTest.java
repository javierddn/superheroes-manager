package com.javierddn.superheroesmanager.domain.port;

import com.javierddn.superheroesmanager.domain.entity.SuperheroEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@SpringBootTest
class SuperheroesRepositoryTest {

    @Autowired
    private PlatformTransactionManager platformTransactionManager;

    @Autowired
    private SuperheroesRepository superheroesRepository;

    @Test
    void givenISaveASuperheroThenICanGetIt() {
        // Given
        TransactionTemplate transactionTemplate = new TransactionTemplate(platformTransactionManager);
        UUID id = UUID.randomUUID();
        SuperheroEntity superheroEntity = new SuperheroEntity.Builder().id(id).name("Ifrit").build();

        // When
        transactionTemplate.execute(s -> superheroesRepository.save(superheroEntity));
        Optional<SuperheroEntity> optionalSuperheroEntity = transactionTemplate.execute(s -> superheroesRepository.findById(id));

        // Then
        assertThat(optionalSuperheroEntity, is(notNullValue()));
        SuperheroEntity result = optionalSuperheroEntity.orElse(null);
        assertThat(result, is(not(nullValue())));
        assertThat(result.getId(), is(id));
        assertThat(result.getName(), is("Ifrit"));
    }
}