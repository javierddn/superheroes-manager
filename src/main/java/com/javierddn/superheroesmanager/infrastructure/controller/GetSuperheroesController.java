package com.javierddn.superheroesmanager.infrastructure.controller;

import com.javierddn.superheroesmanager.domain.entity.SuperheroEntity;
import com.javierddn.superheroesmanager.domain.port.GetSuperheroesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
public class GetSuperheroesController {

    private final GetSuperheroesService getSuperheroesService;

    @Autowired
    public GetSuperheroesController(GetSuperheroesService getSuperheroesService) {
        this.getSuperheroesService = getSuperheroesService;
    }

    @GetMapping("/superheroes")
    public List<SuperheroEntity> getAllSuperheroes() {
        return getSuperheroesService.getAll();
    }

    @GetMapping("/superhero/{id}")
    public SuperheroEntity findSuperheroById(@PathVariable String id) {
        return getSuperheroesService.findById(UUID.fromString(id));
    }

    @GetMapping("/superheroes/{fragmentOfName}")
    public List<SuperheroEntity> get(@PathVariable String fragmentOfName) {
        return getSuperheroesService.findAllWithStringInName(fragmentOfName);
    }
}
