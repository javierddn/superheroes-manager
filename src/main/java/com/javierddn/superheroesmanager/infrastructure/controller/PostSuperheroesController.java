package com.javierddn.superheroesmanager.infrastructure.controller;

import com.javierddn.superheroesmanager.domain.entity.SuperheroEntity;
import com.javierddn.superheroesmanager.domain.port.CreateSuperheroesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PostSuperheroesController {

    private final CreateSuperheroesService createSuperheroesService;

    @Autowired
    public PostSuperheroesController(CreateSuperheroesService createSuperheroesService) {
        this.createSuperheroesService = createSuperheroesService;
    }

    @PostMapping("/superhero")
    public SuperheroEntity findSuperheroById(@RequestBody SuperheroEntity superheroEntity) {
        return createSuperheroesService.save(superheroEntity);
    }
}
