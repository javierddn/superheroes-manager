package com.javierddn.superheroesmanager.infrastructure.controller;

import com.javierddn.superheroesmanager.domain.port.RemoveSuperheroesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
public class DeleteSuperheroesController {

    private final RemoveSuperheroesService removeSuperheroesService;

    @Autowired
    public DeleteSuperheroesController(RemoveSuperheroesService removeSuperheroesService) {
        this.removeSuperheroesService = removeSuperheroesService;
    }

    @DeleteMapping("/superhero/{id}")
    public String deleteSuperheroById(@PathVariable String id) {
        removeSuperheroesService.remove(UUID.fromString(id));
        return "OK";
    }
}
