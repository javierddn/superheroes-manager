package com.javierddn.superheroesmanager.infrastructure.controller;

import com.javierddn.superheroesmanager.domain.entity.SuperheroEntity;
import com.javierddn.superheroesmanager.domain.port.UpdateSuperheroesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
public class PutSuperheroesController {

    private final UpdateSuperheroesService updateSuperheroesService;

    @Autowired
    public PutSuperheroesController(UpdateSuperheroesService updateSuperheroesService) {
        this.updateSuperheroesService = updateSuperheroesService;
    }

    @PutMapping("/superhero")
    public SuperheroEntity findSuperheroById(@RequestBody SuperheroEntity superheroEntity) {
        return updateSuperheroesService.update(superheroEntity);
    }

}
