package com.javierddn.superheroesmanager.application.service;

import com.javierddn.superheroesmanager.domain.port.RemoveSuperheroesService;
import com.javierddn.superheroesmanager.domain.port.SuperheroesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class RemoveSuperheroesServiceImpl implements RemoveSuperheroesService {

    private final SuperheroesRepository repository;

    @Autowired
    public RemoveSuperheroesServiceImpl(SuperheroesRepository repository) {
        this.repository = repository;
    }

    @Override
    public void remove(UUID id) {
        repository.deleteById(id);
    }
}
