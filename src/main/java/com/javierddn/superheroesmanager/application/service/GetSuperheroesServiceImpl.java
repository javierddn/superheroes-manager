package com.javierddn.superheroesmanager.application.service;

import com.javierddn.superheroesmanager.domain.entity.SuperheroEntity;
import com.javierddn.superheroesmanager.domain.port.GetSuperheroesService;
import com.javierddn.superheroesmanager.domain.port.SuperheroesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class GetSuperheroesServiceImpl implements GetSuperheroesService {

    private final SuperheroesRepository repository;

    @Autowired
    public GetSuperheroesServiceImpl(SuperheroesRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<SuperheroEntity> getAll() {
        return repository.findAll();
    }

    @Override
    public SuperheroEntity findById(UUID id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public List<SuperheroEntity> findAllWithStringInName(String fragmentOfName) {
        List<SuperheroEntity> superheroEntities = repository.findAll();
        return superheroEntities
                .stream()
                .filter(s -> s.getName().toLowerCase().contains(fragmentOfName.toLowerCase()))
                .collect(Collectors.toList());
    }
}
