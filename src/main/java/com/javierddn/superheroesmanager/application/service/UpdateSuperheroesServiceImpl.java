package com.javierddn.superheroesmanager.application.service;

import com.javierddn.superheroesmanager.domain.entity.SuperheroEntity;
import com.javierddn.superheroesmanager.domain.port.SuperheroesRepository;
import com.javierddn.superheroesmanager.domain.port.UpdateSuperheroesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UpdateSuperheroesServiceImpl implements UpdateSuperheroesService {

    private final SuperheroesRepository repository;

    @Autowired
    public UpdateSuperheroesServiceImpl(SuperheroesRepository repository) {
        this.repository = repository;
    }

    @Override
    public SuperheroEntity update(SuperheroEntity superheroEntity) {
        return repository.save(superheroEntity);
    }
}
