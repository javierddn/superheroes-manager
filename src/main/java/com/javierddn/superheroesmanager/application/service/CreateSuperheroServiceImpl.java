package com.javierddn.superheroesmanager.application.service;

import com.javierddn.superheroesmanager.domain.entity.SuperheroEntity;
import com.javierddn.superheroesmanager.domain.port.CreateSuperheroesService;
import com.javierddn.superheroesmanager.domain.port.SuperheroesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CreateSuperheroServiceImpl implements CreateSuperheroesService {

    private final SuperheroesRepository repository;

    @Autowired
    public CreateSuperheroServiceImpl(SuperheroesRepository repository) {
        this.repository = repository;
    }

    @Override
    public SuperheroEntity save(SuperheroEntity superheroEntity) {
        return repository.save(superheroEntity);
    }
}
