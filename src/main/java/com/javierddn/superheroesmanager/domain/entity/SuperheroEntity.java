package com.javierddn.superheroesmanager.domain.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "superheroes")
public class SuperheroEntity {

    @Id
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @Column(name = "name", nullable = false)
    private String name;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SuperheroEntity)) return false;
        SuperheroEntity that = (SuperheroEntity) o;
        return new org.apache.commons.lang3.builder.EqualsBuilder()
                .append(id, that.id)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new org.apache.commons.lang3.builder.HashCodeBuilder(7, 9)
                .append(id)
                .toHashCode();
    }

    public static class Builder {

        private final SuperheroEntity object;

        public Builder() {
            object = new SuperheroEntity();
        }

        public Builder id(UUID value) {
            object.id = value;
            return this;
        }

        public Builder name(String value) {
            object.name = value;
            return this;
        }

        public SuperheroEntity build() {
            return object;
        }
    }
}
