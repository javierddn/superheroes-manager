package com.javierddn.superheroesmanager.domain.port;

import com.javierddn.superheroesmanager.domain.entity.SuperheroEntity;

public interface CreateSuperheroesService {

    SuperheroEntity save(SuperheroEntity superheroEntity);
}
