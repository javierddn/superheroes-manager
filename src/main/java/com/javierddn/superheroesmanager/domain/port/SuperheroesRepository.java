package com.javierddn.superheroesmanager.domain.port;

import com.javierddn.superheroesmanager.domain.entity.SuperheroEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface SuperheroesRepository extends JpaRepository<SuperheroEntity, UUID> {
}
