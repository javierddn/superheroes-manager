package com.javierddn.superheroesmanager.domain.port;

import com.javierddn.superheroesmanager.domain.entity.SuperheroEntity;

import java.util.List;
import java.util.UUID;

public interface GetSuperheroesService {

    List<SuperheroEntity> getAll();

    SuperheroEntity findById(UUID id);

    List<SuperheroEntity> findAllWithStringInName(String fragmentOfName);

}
