package com.javierddn.superheroesmanager.domain.port;

import com.javierddn.superheroesmanager.domain.entity.SuperheroEntity;

public interface UpdateSuperheroesService {
    SuperheroEntity update(SuperheroEntity superheroEntity);
}
