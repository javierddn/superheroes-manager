package com.javierddn.superheroesmanager.domain.port;

import java.util.UUID;

public interface RemoveSuperheroesService {

    void remove(UUID id);

}
