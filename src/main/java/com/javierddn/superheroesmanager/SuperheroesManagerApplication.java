package com.javierddn.superheroesmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SuperheroesManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SuperheroesManagerApplication.class, args);
    }

}
