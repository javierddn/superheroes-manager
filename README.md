# H2 Adapter

Simple app to manage a superheroes database

[![Quality gate](https://sonarcloud.io/api/project_badges/quality_gate?project=javierddn_superheroes-manager)](https://sonarcloud.io/dashboard?id=javierddn_superheroes-manager)

This project uses the [SonarScanner for Maven](https://redirect.sonarsource.com/doc/install-configure-scanner-maven.html) to trigger the analysis. Bitbucket Pipelines is configured to build and analyze all branches and pull requests.

## Setting up 🚀

_How to get a copy of this project on your local environment for development and tests proposes[...].

See **Deployment** to know how to deploy the project.

### Requirements 📋

_Requirement software and how to install it [...]_

```
Example[...]
```

### Installing 🔧

_Step by step needed to have the development environment running.[...]_

_About this steep[...]_

```
Command or example[...]
```

_Again[...]_

```
Command or example[...]
```

_Example about how to get the system data or how to use it for a demo[...]_



## Running tests ⚙️

_How to run unit and integration tests[...]_

### End-to-end tests 🔩

_What this tests verify and why[...]_

```
Example[...]
```

### Coding and rules tests ⌨️

_What this tests verify and why[...]_

```
Example[...]
```


## Deployment 📦

_Notes to deploy[...]_


## Environment 💻

_Macbook Pro 16" :_
* Processor: 2,4 GHz Intel Core i9 8 cores
* RAM: 64 GB 2667 MHz DDR4
* Graphics: AMD Radeon Pro 5600M 8 GB + Intel UHD Graphics 630 1536 MB
* macOS Big Sur version 11.2.2

_Monitor: Samsung Odyssey G9_

_IntelliJ IDEA Ultimate 2020.3_


## Built with 🛠️

_Developed with DDD paradigm (hexagonal architecture), reactive programming and TDD_

* [Java 11](https://adoptopenjdk.net) - OpenJDK 11 OpenJ9 version of AdoptOpenJDK
* [Apache Maven 3.6.3](https://maven.apache.org/) - Dependencies manager
* [Spring Boot 2.4.3](https://spring.io/projects/spring-boot) - Transform Spring base Apps in a "Stand Alone-Just Run App"
* [Git](https://git-scm.com) - version control software
* [Bitbucket](https://bitbucket.org/javierddn/superheroes-management-domain) - Git code repository
* [...]

Base project was generated using [Spring Initializr](https://start.spring.io)


## Contributing 🖇️

Please read [CONTRIBUTING.md](https://bitbucket.org/javierddn/workspace/snippets/EpEBoL) for details and send pull requests[...].

Existing [JIRA tickets](https://javierddn.atlassian.net/jira/software/projects/SM/boards/2)

## Documentation 📖

Much more about this project at [Confluence](https://javierddn.atlassian.net/wiki/spaces/SM/overview?homepageId=229451)


## Versioning 📌

Versioning using [SemVer](http://semver.org/) rules. To review all available versions, see [tags](https://bitbucket.org/javierddn/superheroes-management-domain) list in branch select.


## Authors ✒️

* **Javier de Diego Navarro** - *Analysis* - [javierddn](https://bitbucket.org/javierddn)
* **Javier de Diego Navarro** - *Development* - [javierddn](https://bitbucket.org/javierddn)
* **Javier de Diego Navarro** - *[...]* - [javierddn](https://bitbucket.org/javierddn)

Full [contributors](https://bitbucket.org/javierddn/superheroes-management-domain/addon/com.stiltsoft.stash.graphs/graphs-repo-page#!graph=contributors&type=c&group=days) list who have participated in this project.


## License 📄

This project is under the license (GNU General Public License v3.0) - please see [LICENSE.md](LICENSE.md) for details


## Gratitude 🎁

* Share 📢
* Comment for improvements (security bugs, performance, etc) ☠️
* [...].


---
⌨️ with ❤️ by [Javierddn]() 😊

- [LinkedIn](https://www.linkedin.com/in/javierdediegonavarro)
- [Github](https://github.com/javierddn)